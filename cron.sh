#!/bin/bash

set -eu

cd /app/code

# https://easyappointments.org/2021/02/22/configuring-cron-jobs/

echo "=> Run sync task"
/usr/local/bin/gosu www-data:www-data php index.php console sync > '/dev/null' 2>&1
