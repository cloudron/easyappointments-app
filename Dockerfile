FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN a2disconf other-vhosts-access-log && a2enmod rewrite env
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/easyappointments.conf /etc/apache2/sites-enabled/easyappointments.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 5G && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 5G && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/8.1/cli/php.ini Session session.save_path /run/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# renovate: datasource=github-releases depName=alextselegidis/easyappointments versioning=semver
ARG EASYAPPOINTMENTS_VERSION=1.5.1

RUN curl -LSs https://github.com/alextselegidis/easyappointments/releases/download/${EASYAPPOINTMENTS_VERSION}/easyappointments-${EASYAPPOINTMENTS_VERSION}.zip -o easyappointments.zip && \
    unzip easyappointments.zip && rm easyappointments.zip

RUN mv /app/code/config-sample.php /app/pkg/config-sample.php && ln -s /app/data/config.php /app/code/config.php && \
    rm -rf /app/code/storage/sessions && ln -s /run/easyappointments/sessions /app/code/storage/sessions && \
    rm -rf /app/code/storage/logs && ln -s /run/easyappointments/logs /app/code/storage/logs

COPY email.php /app/code/application/config/email.php
COPY start.sh cron.sh /app/pkg/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/pkg/start.sh" ]
