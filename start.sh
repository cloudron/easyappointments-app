#!/bin/bash

set -eu

readonly mysql="mysql -u ${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} -h ${CLOUDRON_MYSQL_HOST} --port ${CLOUDRON_MYSQL_PORT} --database ${CLOUDRON_MYSQL_DATABASE}"

echo "=> Ensure directories"
mkdir -p /run/easyappointments/sessions /app/data /run/easyappointments/logs

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/config.php ]]; then
    echo "=> Ensure config.php"
    cp /app/pkg/config-sample.php /app/data/config.php
fi

echo "=> Patch config.php"
sed -e "s,const BASE_URL.*,const BASE_URL = '${CLOUDRON_APP_ORIGIN}';," -i /app/data/config.php
sed -e "s,const DB_HOST.*,const DB_HOST = '${CLOUDRON_MYSQL_HOST}';," -i /app/data/config.php
sed -e "s,const DB_NAME.*,const DB_NAME = '${CLOUDRON_MYSQL_DATABASE}';," -i /app/data/config.php
sed -e "s,const DB_USERNAME.*,const DB_USERNAME = '${CLOUDRON_MYSQL_USERNAME}';," -i /app/data/config.php
sed -e "s,const DB_PASSWORD.*,const DB_PASSWORD = '${CLOUDRON_MYSQL_PASSWORD}';," -i /app/data/config.php

table_count=$($mysql -NB -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${CLOUDRON_MYSQL_DATABASE}';" 2>/dev/null)

if [[ "${table_count}" == "0" ]]; then
    echo "=> Initial setup"
    sudo -E -u www-data php index.php console install
else
    echo "=> Migrate database"
    sudo -E -u www-data php index.php console migrate
fi

echo "=> Ensure company email and name"
$mysql -e "UPDATE ea_settings SET value='${CLOUDRON_MAIL_SMTP_USERNAME}' WHERE name='company_email'"
$mysql -e "UPDATE ea_settings SET value='${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Easy!Appointments}' WHERE name='company_name'"

echo "=> Ensure permissions"
chown -R www-data.www-data /app/data /run/easyappointments

echo "=> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
