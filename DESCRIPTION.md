## About

Easy!Appointments is a web appointment scheduler.

Users will be able to reach the application through their web browsers by using an active internet connection, just like visiting a normal website.
It allows your customers to book appointments with you via the web.

Moreover, it provides the ability to sync your data with Google Calendar so you can use them with other services.

## Easy To Use

Made with simplicity in mind Easy!Appointments feels directly familiar, with the right actions placed in the right positions you will only have to concentrate on your work.

## Performance

Actively maintained by experienced developers and battle tested by big organizations, Easy!Appointments has an ecosystem you can trust.

## Highly Customizable

Easy!Appointments has a very flexible and versatile codebase that can be customized to serve custom use cases and external system integrations.
